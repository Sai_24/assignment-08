#include<stdio.h>
#define MAX 100

struct student {
	char fname[MAX];
	char subject[MAX];
	int mark;
};

int main(){
 
	struct student s[5];	

	for(int i=0;i<5;i++){
		printf("STUDENT %d \n",i+1);
		printf("Enter the first name of the student: ");
		scanf("%s",&s[i].fname);
		printf("Enter the subject: ");
		scanf("%s",&s[i].subject);
		printf("Marks: ");
		scanf("%d",&s[i].mark);
		printf("\n");
	}
	
	printf("\n STUDENT DETAILS \n");
	printf("\n");

	for(int i=0;i<5;i++){
		printf("STUDENT %d \n",i+1);
		printf("Name: %s \n",s[i].fname);
		printf("Subject: %s \n",s[i].subject);
		printf("Marks: %d \n",s[i].mark);
		printf("\n");
	}

}